# FEM using cyclic symmetry model

M. TANAKA  
created:  2023/08/29  
modified: 2023/12/25  

[TOC]

## 1. やりたいこと

FreeCADのFEMワークベンチには，周期境界条件の設定を行うインターフェースがない．
一方で，FreeCADが利用しているCalculix自体には，`*CYCLIC SYMMETRY MODEL`という設定があることが分かっている．
つまり，自動生成した`inp`ファイルを適切に編集すれば所望の解析を実現できるはずである．
ここでは，FreeCAD＋手作業で周期境界条件での構造解析を実現する．

## 2. 解析対象

等速回転する円盤の静解析を行う．実際には1/12の領域のみで解析する．
結果はvtkにエクスポートしている．
ParaViewを起動して，Load Stateから`paraview_state.py`を読み込めば確認できる．

![](./images/paraview.png)

## 3. 実行方法

- 前提：FreeCADおよびCalculixがインストールされていること．
- シェルで下記コマンドを実行．

```bash
freecadcmd fem_simple_cyclic.py
```

## 4. 手順メモ

### 4.1 動かしかた（上記実行後にGUIのFreeCADで結果を確認する方法？）

1. FCStdファイル開く
2. SolverCcxToolsダブルクリック
3. inpファイルの書き出し（`ccx_default.inp`と同じものが作られる）
4. inpファイルの編集（`ccx_modified.inp`をコピペする）
5. 解析実行

### 4.2. サーフェスの明示

自動生成されたinpファイル内で，マスター・スレーブとなる２つの合同なサーフェスが既に定義されている状態にしたい．
そのため，ダミーの拘束条件をこれらのサーフェスに与える．
とりあえず，x軸に接している方をマスターとする．

- マスター：変位xを固定．
- スレーブ：変位yを固定．

inpファイルには`*NSET`として

- マスター：`ConstraintDisplacement002`
- スレーブ：`ConstraintDisplacement003`

が作成された．仮の拘束としては

```
*BOUNDARY
ConstraintDisplacement002,1

*BOUNDARY
ConstraintDisplacement003,2
```

が作成された．この箇所を変更したい．

### 4.3 inpの書き換え自動化

実現すべき機能は，
- [x] スレーブ面のうち変位拘束が過重にかかっているエッジを構成するノードの特定（今回は`NSET=TemporalConstraintReplicaEdge`）
    - Pythonのsetの形でノード番号を取得したい
    - `*NSET,NSET=TemporalConstraintReplicaEdge`に続く行が`[0-9]*,$`を満たす限りsetに追加する

- [x] 変位の過重拘束を掛けているリスト（今回は`NSET=ConstraintBottomFace`）から，上記エッジに対応するノードを除去
    - 難しそう
    - これもPythonのsetとして取り込んで，Python内で差分を取る
    ```python
    set1 = {"A", "B", "C", "D"}
    set2 = {"C", "D", "E", "F"}

    print(set1 - set2)
    >> {'A', 'B'}
    ```

    - inpファイルのリストは一旦削除して，再び挿入するとか

- [x] `*SURFACE`, `*TIE`, `*CYCLIC SYMMETRY MODEL`の追記
    - 書式は決まり切っているので簡単そう
    - `*STEP`の上の行に挿入するにはどうすれば良いか？
        - listのスライスやらなんやらで対処．

- [x] 仮に設けた拘束の除去

- [x] 仮の回転軸の修正
    - `^ConstraintCentrif`の行を書き換え．
    - 100Hz が394784.1760436になるが，どういう単位？？？


## 5. 気になること

- GUIで操作する場合は`doc.CCX_Results`の下に`doc.CCX_Results_Mesh`が生成されるのだが，CUIで行うとMeshが外にできる．このため，FreeCAD上で結果を可視化することができない．

    - vtkにエクスポートすればParaViewで可視化はできるので，一応問題にはならないが，気にはなる．

- `> freecadcmd fem_simple_cyclic.py`ではそうだが，`> freecad fem_simple_cyclic.py`とGUIアプリに同じpyファイルを読ませると正しくツリーが生成された．今回の場合あくまでコンソールで実行したいので助けにはならないが，覚えておこう．


## 6. エラーメモ

```
*ERROR in cascade: the DOF corresponding to 
node 3 in direction 1 is detected on the 
dependent side of a MPC and a SPC
```

マスタースレーブの２つのサーフェスに共通ノードがあるせい？
でもそんな例はいくらでもあるしなあ（ピザのイメージ）
検証：バームクーヘン型に変えてみる．
結果：同じエラーでた．

- PrePoMaxで動くケースつくってinp見ればいいのでは？←試す．
    - CYCLIC SYMMETRY MODEL対応してなかった．．．

これと同じっぽい：
- https://calculix.discourse.group/t/cyclic-symmetry-error-when-slave-master-contain-some-nodes-from-fixed-surface
- 書いてあるとおり拘束のリストからスレーブ面に属するノード番号を除去すると動いた！

## 7. TO DO

- [ ] 円盤が回転しないように接点に拘束条件を与えたが，そのせいで応力集中が生じている．これはなくても計算が収束したはずなので，除去する．

## 8. 参考

### 外部資料

- https://classes.engineering.wustl.edu/2009/spring/mase5513/abaqus/docs/v6.5/books/ver/default.htm?startat=ch03s09abv213.html

- https://calculix.discourse.group/

### 自作資料

- https://gitlab.com/freecadscripts/fem-with-freecadcmd