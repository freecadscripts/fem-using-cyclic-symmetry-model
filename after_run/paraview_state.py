# state file generated using paraview version 5.10.1

import os

# uncomment the following three lines to ensure this script works in future versions
#import paraview
#paraview.compatibility.major = 5
#paraview.compatibility.minor = 10

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [1215, 773]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.CenterOfRotation = [0.0, 0.0, 5.0]
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [11.180881623764629, -506.93866880646317, 504.6651116362396]
renderView1.CameraFocalPoint = [-2.122469677472753, 2.1493035701273095, -41.188273958914216]
renderView1.CameraViewUp = [4.6006948287949385e-05, 0.7313065087518738, 0.682048963154777]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 282.8869031963127
renderView1.BackEnd = 'OSPRay raycaster'
renderView1.OSPRayMaterialLibrary = materialLibrary1

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)
layout1.SetSize(1215, 773)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'Legacy VTK Reader'
dir = os.path.dirname(__file__)
simple_cyclicCCX_Results_Meshvtk = LegacyVTKReader(registrationName='simple_cyclic-CCX_Results_Mesh.vtk', FileNames=[f'{dir}/simple_cyclic-CCX_Results_Mesh.vtk'])

# create a new 'Warp By Vector'
warpByVector1 = WarpByVector(registrationName='WarpByVector1', Input=simple_cyclicCCX_Results_Meshvtk)
warpByVector1.Vectors = ['POINTS', 'Displacement']
warpByVector1.ScaleFactor = 1000000.0

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from simple_cyclicCCX_Results_Meshvtk
simple_cyclicCCX_Results_MeshvtkDisplay = Show(simple_cyclicCCX_Results_Meshvtk, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
simple_cyclicCCX_Results_MeshvtkDisplay.Representation = 'Surface'
simple_cyclicCCX_Results_MeshvtkDisplay.ColorArrayName = [None, '']
simple_cyclicCCX_Results_MeshvtkDisplay.Opacity = 0.57
simple_cyclicCCX_Results_MeshvtkDisplay.SelectTCoordArray = 'None'
simple_cyclicCCX_Results_MeshvtkDisplay.SelectNormalArray = 'None'
simple_cyclicCCX_Results_MeshvtkDisplay.SelectTangentArray = 'None'
simple_cyclicCCX_Results_MeshvtkDisplay.OSPRayScaleArray = 'Displacement'
simple_cyclicCCX_Results_MeshvtkDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
simple_cyclicCCX_Results_MeshvtkDisplay.SelectOrientationVectors = 'Displacement'
simple_cyclicCCX_Results_MeshvtkDisplay.ScaleFactor = 40.0
simple_cyclicCCX_Results_MeshvtkDisplay.SelectScaleArray = 'Displacement'
simple_cyclicCCX_Results_MeshvtkDisplay.GlyphType = 'Arrow'
simple_cyclicCCX_Results_MeshvtkDisplay.GlyphTableIndexArray = 'Displacement'
simple_cyclicCCX_Results_MeshvtkDisplay.GaussianRadius = 2.0
simple_cyclicCCX_Results_MeshvtkDisplay.SetScaleArray = ['POINTS', 'Displacement']
simple_cyclicCCX_Results_MeshvtkDisplay.ScaleTransferFunction = 'PiecewiseFunction'
simple_cyclicCCX_Results_MeshvtkDisplay.OpacityArray = ['POINTS', 'Displacement']
simple_cyclicCCX_Results_MeshvtkDisplay.OpacityTransferFunction = 'PiecewiseFunction'
simple_cyclicCCX_Results_MeshvtkDisplay.DataAxesGrid = 'GridAxesRepresentation'
simple_cyclicCCX_Results_MeshvtkDisplay.PolarAxes = 'PolarAxesRepresentation'
simple_cyclicCCX_Results_MeshvtkDisplay.ScalarOpacityUnitDistance = 34.73880873093499
simple_cyclicCCX_Results_MeshvtkDisplay.OpacityArrayName = ['POINTS', 'Displacement']

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
simple_cyclicCCX_Results_MeshvtkDisplay.ScaleTransferFunction.Points = [-2.74709e-05, 0.0, 0.5, 0.0, 2.74709e-05, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
simple_cyclicCCX_Results_MeshvtkDisplay.OpacityTransferFunction.Points = [-2.74709e-05, 0.0, 0.5, 0.0, 2.74709e-05, 1.0, 0.5, 0.0]

# show data from warpByVector1
warpByVector1Display = Show(warpByVector1, renderView1, 'UnstructuredGridRepresentation')

# get color transfer function/color map for 'vonMisesStress'
vonMisesStressLUT = GetColorTransferFunction('vonMisesStress')
vonMisesStressLUT.AutomaticRescaleRangeMode = 'Never'
vonMisesStressLUT.RGBPoints = [20000000.0, 0.0, 0.0, 1.0, 100000000.0, 1.0, 0.0, 0.0]
vonMisesStressLUT.ColorSpace = 'HSV'
vonMisesStressLUT.NanColor = [0.498039215686, 0.498039215686, 0.498039215686]
vonMisesStressLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'vonMisesStress'
vonMisesStressPWF = GetOpacityTransferFunction('vonMisesStress')
vonMisesStressPWF.Points = [20000000.0, 0.0, 0.5, 0.0, 100000000.0, 1.0, 0.5, 0.0]
vonMisesStressPWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
warpByVector1Display.Representation = 'Surface With Edges'
warpByVector1Display.ColorArrayName = ['POINTS', 'von Mises Stress']
warpByVector1Display.LookupTable = vonMisesStressLUT
warpByVector1Display.SelectTCoordArray = 'None'
warpByVector1Display.SelectNormalArray = 'None'
warpByVector1Display.SelectTangentArray = 'None'
warpByVector1Display.OSPRayScaleArray = 'Displacement'
warpByVector1Display.OSPRayScaleFunction = 'PiecewiseFunction'
warpByVector1Display.SelectOrientationVectors = 'Displacement'
warpByVector1Display.ScaleFactor = 47.93976440429688
warpByVector1Display.SelectScaleArray = 'Displacement'
warpByVector1Display.GlyphType = 'Arrow'
warpByVector1Display.GlyphTableIndexArray = 'Displacement'
warpByVector1Display.GaussianRadius = 2.3969882202148436
warpByVector1Display.SetScaleArray = ['POINTS', 'Displacement']
warpByVector1Display.ScaleTransferFunction = 'PiecewiseFunction'
warpByVector1Display.OpacityArray = ['POINTS', 'Displacement']
warpByVector1Display.OpacityTransferFunction = 'PiecewiseFunction'
warpByVector1Display.DataAxesGrid = 'GridAxesRepresentation'
warpByVector1Display.PolarAxes = 'PolarAxesRepresentation'
warpByVector1Display.ScalarOpacityFunction = vonMisesStressPWF
warpByVector1Display.ScalarOpacityUnitDistance = 41.63183275759534
warpByVector1Display.OpacityArrayName = ['POINTS', 'Displacement']

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
warpByVector1Display.ScaleTransferFunction.Points = [-2.74709e-05, 0.0, 0.5, 0.0, 2.74709e-05, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
warpByVector1Display.OpacityTransferFunction.Points = [-2.74709e-05, 0.0, 0.5, 0.0, 2.74709e-05, 1.0, 0.5, 0.0]

# setup the color legend parameters for each legend in this view

# get color legend/bar for vonMisesStressLUT in view renderView1
vonMisesStressLUTColorBar = GetScalarBar(vonMisesStressLUT, renderView1)
vonMisesStressLUTColorBar.Title = 'von Mises Stress'
vonMisesStressLUTColorBar.ComponentTitle = ''

# set color bar visibility
vonMisesStressLUTColorBar.Visibility = 1

# show color legend
warpByVector1Display.SetScalarBarVisibility(renderView1, True)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# restore active source
SetActiveSource(warpByVector1)
# ----------------------------------------------------------------


if __name__ == '__main__':
    # generate extracts
    SaveExtracts(ExtractsOutputDirectory='extracts')