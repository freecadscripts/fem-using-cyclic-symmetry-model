import FreeCAD as App
import Part
import Sketcher
import ObjectsFem
import feminout.importVTKResults
from femtools import ccxtools

import math
import os
import shutil

# my own module
import modify_inp

name = 'simple_cyclic'
doc = App.newDocument(name)

# Create body
print('# Create body...')
body = doc.addObject('PartDesign::Body','Body')

## Create sketch
print('    # Create sketch...')
sketch = body.newObject('Sketcher::SketchObject','Sketch')
sketch.Support = (doc.getObject('XY_Plane'),[''])
sketch.MapMode = 'FlatFace'
doc.recompute()

START = 1
END = 2
CENTER = 3
XAXIS = -1
YAXIS = -2

r1 = 50.0 # mm
r2 = 200.0 # mm
th = math.radians(30.0) # rad (=30 deg)

geo = []
geo.append(
    sketch.addGeometry(
        Part.ArcOfCircle(
            Part.Circle(App.Vector(0.0,0.0,0.0),App.Vector(0,0,1), r1),
            0.000000,
            th
        ),
        False
    )
)
sketch.addConstraint(Sketcher.Constraint('Coincident', geo[0],CENTER, XAXIS,START)) 
sketch.addConstraint(Sketcher.Constraint('PointOnObject', geo[0],START, XAXIS)) 

geo.append(
    sketch.addGeometry(
        Part.ArcOfCircle(
            Part.Circle(App.Vector(0.0,0.0,0.0),App.Vector(0,0,1), r2),
            0.000000,
            th
        ),
        False
    )
)
sketch.addConstraint(Sketcher.Constraint('Coincident', geo[1],CENTER, geo[0],CENTER)) 
sketch.addConstraint(Sketcher.Constraint('PointOnObject', geo[1],START, XAXIS)) 

geo.append(
    sketch.addGeometry(
        Part.LineSegment(
            App.Vector(r1,0.0,0.0),
            App.Vector(r2,0.0,0.0)
        ),
        False
    )
)
sketch.addConstraint(Sketcher.Constraint('Coincident',geo[2],START,geo[0],START)) 
sketch.addConstraint(Sketcher.Constraint('Coincident',geo[2],END,geo[1],START)) 

geo.append(
    sketch.addGeometry(
        Part.LineSegment(
            App.Vector(r1*math.cos(th), r1*math.sin(th), 0.0),
            App.Vector(r2*math.cos(th), r2*math.sin(th), 0.0)
        ),
        False
    )
)
sketch.addConstraint(Sketcher.Constraint('Coincident',geo[3],START,geo[0],END)) 
sketch.addConstraint(Sketcher.Constraint('Coincident',geo[3],END,geo[1],END)) 
sketch.addConstraint(Sketcher.Constraint('PointOnObject',geo[0],CENTER, geo[3]))

n = sketch.addConstraint(Sketcher.Constraint('Radius',geo[0],r1))
# sketch.renameConstraint(n, 'r1')
# sketch.setDatum('r1', App.Units.Quantity('50.0 mm'))
n = sketch.addConstraint(Sketcher.Constraint('Radius',geo[1],r2))
# sketch.renameConstraint(n, 'r2')
# sketch.setDatum('r2', App.Units.Quantity('200.0 mm'))
n = sketch.addConstraint(Sketcher.Constraint('Angle',geo[2],START,geo[3],START,th))
# sketch.renameConstraint(n, 'th')
# sketch.setDatum('th', App.Units.Quantity('30.0 deg'))

doc.recompute()


## Create Pad
print('    # Create pad...')
pad = body.newObject('PartDesign::Pad','Pad')
pad.Profile = sketch
pad.ReferenceAxis = (sketch,['N_Axis'])
pad.Length = 10.0
pad.TaperAngle = 0.0
pad.UseCustomVector = 0
pad.Direction = (0, 0, 1)
pad.AlongSketchNormal = 1
pad.Type = 0
pad.UpToFace = None
pad.Reversed = 0
pad.Midplane = 0
pad.Offset = 0
doc.recompute()


# Create analysis
print('# Create analysis...')
analysis = ObjectsFem.makeAnalysis(doc, 'Analysis')
solverCcxTools = ObjectsFem.makeSolverCalculixCcxTools(doc)
analysis.addObject(solverCcxTools)

## Create material
print('    # Create material...')
material = ObjectsFem.makeMaterialSolid(doc)
material_parameters = {
    'AuthorAndLicense': '(c) 2013 Juergen Riegel (CC-BY 3.0)',
    'CardName': 'Calculix-Steel',
    'Density': '7900 kg/m^3',
    'Description': 'Standard steel material for Calculix sample calculations',
    'Father': 'Metal',
    'Name': 'Calculix-Steel',
    'PoissonRatio': '0.3',
    'SpecificHeat': '590 J/kg/K',
    'ThermalConductivity': '43 W/m/K',
    'ThermalExpansionCoefficient': '0.000012 m/m/K',
    'YoungsModulus': '210000 MPa'
}
material.Material = material_parameters
analysis.addObject(material)

## Create constraints
print('    # Create constraints...')
TOLERANCE = 0.01 # mm, mm^2
faces = body.Shape.Faces
for i, face in enumerate(faces):
    if abs(face.CenterOfMass.z) < TOLERANCE:
        n_face_bottom = i
    elif abs(face.CenterOfMass.y) < TOLERANCE:
        n_face_primary = i
        face_primary = face
    else:
        pass

for i, face in enumerate(faces):
    if abs(face.Area - face_primary.Area) < TOLERANCE and i != n_face_primary:
        n_face_replica = i
        face_replica = face
    else:
        pass

for edge in face_replica.Edges:
    if abs(edge.CenterOfMass.z) < TOLERANCE:
        edge_overconst = edge
    else:
        pass

for i, edge in enumerate(body.Shape.Edges):
    if abs(edge.CenterOfMass.x - r1) < TOLERANCE:
        n_edge_bound = i
    elif edge.CenterOfMass == edge_overconst.CenterOfMass:
        n_edge_overconst = i
    else:
        pass

const_bottom = doc.addObject('Fem::ConstraintDisplacement','ConstraintBottomFace')
analysis.addObject(const_bottom)
const_bottom.xDisplacement = '0.00 mm' # these are really necessary?
const_bottom.xDisplacementFormula = ''
const_bottom.yDisplacement = '0.00 mm'
const_bottom.yDisplacementFormula = ''
const_bottom.zDisplacement = '0.00 mm'
const_bottom.zDisplacementFormula = ''
const_bottom.xRotation = '0.00 °'
const_bottom.yRotation = '0.00 °'
const_bottom.zRotation = '0.00 °'
const_bottom.xFree = True
const_bottom.xFix = False
const_bottom.hasXFormula = False
const_bottom.yFree = True
const_bottom.yFix = False
const_bottom.hasYFormula = False
const_bottom.zFree = False
const_bottom.zFix = False
const_bottom.hasZFormula = False
const_bottom.rotxFree = True
const_bottom.rotxFix = False
const_bottom.rotyFree = True
const_bottom.rotyFix = False
const_bottom.rotzFree = True
const_bottom.rotzFix = False
const_bottom.useFlowSurfaceForce = False
const_bottom.Scale = 11
const_bottom.References = [(body, f'Face{n_face_bottom + 1}')]
doc.recompute()

const_primary_face = doc.addObject('Fem::ConstraintFixed','TemporalConstraintPrimaryFace')
analysis.addObject(const_primary_face)
const_primary_face.Scale = 8
const_primary_face.References = [(body, f'Face{n_face_primary + 1}')]
doc.recompute()

const_replica_face = doc.addObject('Fem::ConstraintFixed','TemporalConstraintReplicaFace')
analysis.addObject(const_replica_face)
const_replica_face.Scale = 8
const_replica_face.References = [(body, f'Face{n_face_replica + 1}')]
doc.recompute()

const_replica_edge = doc.addObject('Fem::ConstraintFixed','TemporalConstraintReplicaEdge')
analysis.addObject(const_replica_edge)
const_replica_edge.Scale = 13
const_replica_edge.References = [(body, f'Edge{n_edge_overconst + 1}')]
doc.recompute()

const_inner_edge = doc.addObject("Fem::ConstraintDisplacement","ConstraintInnerEgde")
analysis.addObject(const_inner_edge)
const_inner_edge.xDisplacement = "0.00 mm"
const_inner_edge.xDisplacementFormula = ""
const_inner_edge.yDisplacement = "0.00 mm"
const_inner_edge.yDisplacementFormula = ""
const_inner_edge.zDisplacement = "0.00 mm"
const_inner_edge.zDisplacementFormula = ""
const_inner_edge.xRotation = "0.00 °"
const_inner_edge.yRotation = "0.00 °"
const_inner_edge.zRotation = "0.00 °"
const_inner_edge.xFree = True
const_inner_edge.xFix = False
const_inner_edge.hasXFormula = False
const_inner_edge.yFree = False
const_inner_edge.yFix = False
const_inner_edge.hasYFormula = False
const_inner_edge.zFree = True
const_inner_edge.zFix = False
const_inner_edge.hasZFormula = False
const_inner_edge.rotxFree = True
const_inner_edge.rotxFix = False
const_inner_edge.rotyFree = True
const_inner_edge.rotyFix = False
const_inner_edge.rotzFree = True
const_inner_edge.rotzFix = False
const_inner_edge.useFlowSurfaceForce = False
const_inner_edge.Scale = 1
const_inner_edge.References = [(body, f'Edge{n_edge_bound + 1}')]
doc.recompute()

analysis.addObject(ObjectsFem.makeConstraintCentrif(doc))
centerif = doc.getObject('ConstraintCentrif')
centerif.RotationFrequency = '100 1/s'
centerif.References = [(body, 'Solid1')]
centerif.RotationAxis = [(body, f'Edge{n_edge_bound + 1}')]
doc.recompute()

## Create mesh
print('    # Create mesh...')
mesh = ObjectsFem.makeMeshNetgen(doc, 'FEMMeshNetgen')
mesh.Shape = body
mesh.MaxSize = 50.0 # mm
mesh.Fineness = 'VeryCoarse' # 'VeryCoarse'|'Coarse'|'Moderate'|'Fine'|'VeryFine'
analysis.addObject(mesh)
doc.recompute()

## Run analysis
print('    # Run analysis...')
fea = ccxtools.FemToolsCcx()
fea.purge_results()
fea.run()

# run the analysis step by step
from femtools import ccxtools
fea = ccxtools.FemToolsCcx()
fea.update_objects()
fea.setup_working_dir()
fea.setup_ccx()
message = fea.check_prerequisites()

dir = os.path.dirname(__file__)
if not message:
    fea.purge_results()
    fea.write_inp_file()
    # on error at inp file writing, the inp file path "" was returned (even if the file was written)
    # if we would write the inp file anyway, we need to again set it manually
    # fea.inp_file_name = '/tmp/FEMWB/FEMMeshGmsh.inp'
    shutil.copy(fea.inp_file_name, f'{dir}/ccx_default.inp')
else:
    App.Console.PrintError("Houston, we have a problem! {}\n".format(message))  # in report view
    print("Houston, we have a problem! {}\n".format(message))  # in Python console

modify_inp.main()
shutil.copy(f'{dir}/ccx_modified.inp', fea.inp_file_name)

fea.ccx_run()
fea.load_results()

ccxResults = doc.CCX_Results
# ccxResultsMesh = doc.CCX_Results_Mesh
# ccxResults.addObject(ccxResultsMesh) # not work.

stress_max = max(ccxResults.vonMises) # MPa
stress_min = min(ccxResults.vonMises) # MPa

print(stress_max)
print(stress_min)

feminout.importVTKResults.export([ccxResults], f'{dir}/{name}-CCX_Results_Mesh.vtk')
doc.saveAs(f'{dir}/{name}.FCStd')
