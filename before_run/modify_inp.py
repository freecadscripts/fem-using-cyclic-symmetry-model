import os
import re

def main() -> None:
    dir = os.path.dirname(__file__)

    with open(f'{dir}/ccx_default.inp')as f:
        data_input = f.readlines()

    flag_edge_overconst = False
    flag_bottom_face = False

    set_overconst_edge = set()
    set_bottom_face = set()

    for i, line in enumerate(data_input):
        if flag_edge_overconst:
            if bool(re.match(r'^[0-9]+,', line)):
                set_overconst_edge.add(line)
            else:
                flag_edge_overconst = False

        if bool(re.match(r'^\*NSET,NSET=TemporalConstraintReplicaEdge$', line)):
            n_nset_overconst = i
            flag_edge_overconst = True
        # NOTE: NSET of 'ConstraintFixed'  will be above that of 'ConstraintDisplacement' in inp file regardless of the order in FCStd file.

        if flag_bottom_face:
            if bool(re.match(r'^[0-9]+,', line)):
                set_bottom_face.add(line)
            else:
                flag_bottom_face = False

        if bool(re.match(r'^\*NSET,NSET=ConstraintBottomFace$', line)):
            n_nset_bottom_face = i
            flag_bottom_face = True

        if bool(re.match(r'^\*SOLID SECTION,', line)):
            n_solidsection = i

        if bool(re.match(r'^\*\* Fixed Constraints$', line)):
            n_constraints_to_be_removed = i

        if bool(re.match(r'^ConstraintCentrif,CENTRIF,', line)):
            n_dload_centrif = i


    data_output = data_input

    # modify data_output FROM BOTTOM TO TOP
    ## CENTRIF: set proper value
    data_output[n_dload_centrif] = 'ConstraintCentrif,CENTRIF,394784.1760436,0,0,0,0,0,1\n'

    ## remove all temporal BOUNDARY entries
    del data_output[n_constraints_to_be_removed - 1 : n_constraints_to_be_removed + 18]
    
    ## add CYCLIC SYMMETRY MODEL
    cyclicsym = [
        '\n',
        '***********************************************************\n',
        '** *CYCLIC SYMMETRY MODEL should be placed before all step definitions\n',
        '*SURFACE, TYPE=NODE, NAME=Primary1\n',
        'TemporalConstraintPrimaryFace\n',
        '\n',
        '*SURFACE, TYPE=NODE, NAME=Replica1\n',
        'TemporalConstraintReplicaFace\n',
        '\n',
        '*TIE, CYCLIC SYMMETRY, NAME=Tie1\n',
        'Replica1, Primary1\n',
        '\n',
        '*CYCLIC SYMMETRY MODEL, N=12, NGRAPH=12, TIE=Tie1\n',
        '0.,0.,0.,0.,0.,1.\n',
        '\n',
    ]
    data_output[n_solidsection + 1 : n_solidsection + 1] = cyclicsym

    ## remove the original nodes of ConstraintDisplacement
    del data_output[n_nset_bottom_face + 1 : n_nset_bottom_face + len(set_bottom_face) + 1]

    ## add the proper nodes of ConstraintDisplacement
    set_diff = set_bottom_face - set_overconst_edge
    data_output[n_nset_bottom_face + 1 : n_nset_bottom_face + 1] = list(set_diff)

    with open(f'{dir}/ccx_modified.inp', mode='w')as f:
        f.writelines(data_output)

    return
