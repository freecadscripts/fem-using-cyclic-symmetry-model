import os
import re


dir = os.path.dirname(__file__)
dir = os.path.dirname(dir)

with open(f'{dir}/ccx_default.inp')as f:
    data_input = f.readlines()

flag_Fixed = False
flag_Displacement = False

set_ConstraintFixed = set()
set_ConstraintDisplacement = set()

for i, line in enumerate(data_input):
    if flag_Fixed:
        if bool(re.match(r'^[0-9]+,', line)):
            set_ConstraintFixed.add(line)
        else:
            flag_Fixed = False

    if bool(re.match(r'^\*NSET,NSET=ConstraintFixed$', line)):
        n_nset_fixed = i
        flag_Fixed = True

    if flag_Displacement:
        if bool(re.match(r'^[0-9]+,', line)):
            set_ConstraintDisplacement.add(line)
        else:
            flag_Displacement = False

    if bool(re.match(r'^\*NSET,NSET=ConstraintDisplacement$', line)):
        n_nset_displacement = i
        flag_Displacement = True

    if bool(re.match(r'^\*SOLID SECTION,', line)):
        n_solidsection = i

    if bool(re.match(r'^ConstraintFixed,1$', line)):
        n_boundary_displacement = i

    if bool(re.match(r'^ConstraintDisplacement001,1$', line)):
        n_boundary_displacement1 = i

    if bool(re.match(r'^ConstraintDisplacement002,2$', line)):
        n_boundary_displacement2 = i

    if bool(re.match(r'^ConstraintCentrif,CENTRIF,', line)):
        n_dload_centrif = i


data_output = data_input

# modify data_output from bottom to top
## CENTRIF: set proper value
data_output[n_dload_centrif] = 'ConstraintCentrif,CENTRIF,394784.1760436,0,0,0,0,0,1\n'

## remove BOUNDARY entries
del data_output[n_boundary_displacement2 - 1 : n_boundary_displacement2 + 1]
del data_output[n_boundary_displacement1 - 1 : n_boundary_displacement1 + 1]
del data_output[n_boundary_displacement - 1 : n_boundary_displacement + 3]

# add CYCLIC SYMMETRY MODEL
cyclicsym = [
    '\n',
    '***********************************************************\n',
    '** *CYCLIC SYMMETRY MODEL should be placed before all step definitions\n',
    '*SURFACE, TYPE=NODE, NAME=Primary1\n',
    'ConstraintDisplacement001\n',
    '\n',
    '*SURFACE, TYPE=NODE, NAME=Replica1\n',
    'ConstraintDisplacement002\n',
    '\n',
    '*TIE, CYCLIC SYMMETRY, NAME=Tie1\n',
    'Replica1, Primary1\n',
    '\n',
    '*CYCLIC SYMMETRY MODEL, N=12, NGRAPH=12, TIE=Tie1\n',
    '0.,0.,0.,0.,0.,1.\n',
    '\n',
    '\n',
]
data_output[n_solidsection + 1 : n_solidsection + 1] = cyclicsym

# remove the original nodes of ConstraintDisplacement
del data_output[n_nset_displacement + 1 : n_nset_displacement + len(set_ConstraintDisplacement) + 1]

# add the proper nodes of ConstraintDisplacement
set_diff = set_ConstraintDisplacement - set_ConstraintFixed
data_output[n_nset_displacement + 1 : n_nset_displacement + 1] = list(set_diff)

with open(f'{dir}/ccx_mod_py.inp', mode='w')as f:
    f.writelines(data_output)