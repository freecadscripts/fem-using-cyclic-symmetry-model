
0.1: ************************************************************ 
CalculiX Version 2.21, Copyright(C) 1998-2023 Guido Dhondt 
CalculiX comes with ABSOLUTELY NO WARRANTY. This is free 
software, and you are welcome to redistribute it under 
certain conditions, see gpl.htm 
************************************************************ 
You are using an executable made on Sat Jul 29 10:52:01 CEST 2023 
The numbers below are estimated upper bounds 
number of: 
nodes: 987 
elements: 701 
one-dimensional elements: 0 
two-dimensional elements: 0 
integration points per element: 4 
degrees of freedom per node: 3 
layers per element: 1 
distributed facial loads: 0 
distributed volumetric loads: 1 
concentrated loads: 0 
single point constraints: 202 
multiple point constraints: 759 
terms in all multiple point constraints: 14709 
tie constraints: 1 
dependent nodes tied by cyclic constraints: 139 
dependent nodes in pre-tension constraints: 0 
sets: 10 
terms in all sets: 3534 
materials: 1 
constants per material and temperature: 2 
temperature points per material: 1 
plastic data points per material: 0 
orientations: 0 
amplitudes: 2 
data points in all amplitudes: 2 
print requests: 3 
transformations: 1 
property cards: 0 
*INFO reading *CYCLIC SYMMETRY MODEL: 
no element set given 
*INFO reading *CYCLIC SYMMETRY MODE. Card image: 
*CYCLICSYMMETRYMODEL,N=12,NGRAPH=3,TIE=TIE1 
*INFO reading *CYCLIC SYMMETRY MODEL: 
no tolerance was defined 
in the *TIE option; a tolerance of 3.4643855051832412E-009 
will be used 
STEP 1 
Static analysis was selected 
Decascading the MPC's 
*ERROR in cascade: the DOF corresponding to 
node 6 in direction 3 is detected on the 
dependent side of a MPC and a SPC
0.2: CalculiX execute error: 
0.2: Loading result sets...
